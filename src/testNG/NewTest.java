package testNG;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class NewTest {
	
	/*@BeforeClass
	public void bc(){

	}*/
	
	@DataProvider
	public WebDriver dp(){
		File file = new File("C:/selenium/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		return new ChromeDriver();
	}
	
  @Test(priority = 4, dataProvider = "dp")
  public void test(WebDriver driver) {
	  driver.get("http://www.google.com");
	  try {
		Thread.sleep(1000);
		} 
		  catch (InterruptedException e) {
			e.printStackTrace();
		}
  }
  
  @AfterTest
  public void afterTest(WebDriver driver) {
	  driver.quit();
  }
}
