package testNG;

import org.testng.annotations.Test;

public class priorityTest {
	
	// the lower the priority the sooner a test gets run
  @Test(priority=0, groups="cycle1")
  public void test1() {
	  System.out.println("Test 1");
  }
  
  @Test(priority=1, groups={"cycle1", "smoke"})
  public void test2() {
	  System.out.println("Test 2");
  }
  
  @Test(priority=-1)
  public void test3() {
	  System.out.println("Test 3");
  }
  
  @Test()
  public void test5() {
	  System.out.println("Test 5");
  }
  
  @Test
  public void test4() {
	  System.out.println("Test 4");
  }
}
