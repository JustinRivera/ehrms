package pom;


import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class testNGwithPOM {
	
	private static WebDriver driver = null;
	
	@BeforeTest
	public void driverSetup(){
		
	}
	
	@BeforeMethod
	public void init(){
		driver = EHRMS.launchApp();
	}
	
	@Test(priority=0)
	public void launch() {
		AssertJUnit.assertEquals("http://192.168.60.11:7001/eHRMS/logout.do", driver.getCurrentUrl());
 	}
	
	@Test(priority=1)
	public void login() {
		LoginPage.loginEHRMS(driver);
		AssertJUnit.assertEquals(driver.getCurrentUrl(), "http://192.168.60.11:7001/eHRMS/adminHomePage.do");
	}
	
	@Test(priority=2)
	public void openManagement() {
		LoginPage.loginEHRMS(driver);
		HomePage.openEmployeeManagement(driver);
		AssertJUnit.assertEquals(driver.getCurrentUrl(), "http://192.168.60.11:7001/eHRMS/leaveRequest.do?method=list");
	}
	
	@Test(priority=3)
	public void newLeave() {
		LoginPage.loginEHRMS(driver);
		HomePage.openEmployeeManagement(driver);
		EmployeeManagementPage.addNewLeave(driver);
		AssertJUnit.assertEquals(driver.getCurrentUrl(), "http://192.168.60.11:7001/eHRMS/saveLeaveRequest.do?method=save");
	}
	
	@Test(priority=4)
	public void openTimeSheet() {
		LoginPage.loginEHRMS(driver);
		HomePage.openEmployeeTimeSheet(driver);
		AssertJUnit.assertEquals(driver.getCurrentUrl(), "http://192.168.60.11:7001/eHRMS/employeesShift.do?method=list");
	}
	
	@Test(priority=5)
	public void newTimeSheet() {
		LoginPage.loginEHRMS(driver);
		HomePage.openEmployeeTimeSheet(driver);
		EmployeeTimeSheetPage.addTimeSheet(driver);
		AssertJUnit.assertEquals(driver.getCurrentUrl() ,"http://192.168.60.11:7001/eHRMS/employeesShift.do?method=add");
	}
	
	@Test(priority=6)
	public void logout() {
		AssertJUnit.assertEquals(driver.getCurrentUrl() ,"http://192.168.60.11:7001/eHRMS/logout.do");
	}
	
	@AfterMethod
	public void close() {
		EHRMS.closeApp(driver);
	}
}
