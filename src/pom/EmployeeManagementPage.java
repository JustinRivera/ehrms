package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EmployeeManagementPage {

	public static WebElement leaveRequest(WebDriver driver){
		return driver.findElement(By.xpath("//*[@id=\"dt_example\"]/table/tbody/tr[2]/td[2]/table/tbody/tr/td/form/table/tbody/tr[3]/td/p/a/font"));
	}
	
	public static WebElement toDate(WebDriver driver) {
		return driver.findElement(By.name("toDate"));
	}
	
	public static WebElement fromDate(WebDriver driver) {
		return driver.findElement(By.name("fromDate"));
	}
	
	public static WebElement reason(WebDriver driver) {
		return driver.findElement(By.id("reason"));
	}
	
	public static WebElement save(WebDriver driver){
		return driver.findElement(By.xpath("//*[@id=\"newemp\"]/div/table/tbody/tr[8]/td[2]/input"));
	}
	
	public static WebElement home(WebDriver driver){
		return driver.findElement(By.xpath("//*[@id=\"accordion\"]/div[1]/a"));
	}
	
	public static void addNewLeave(WebDriver driver){
		leaveRequest(driver).click();
		fromDate(driver).sendKeys("1/5/2017");
		toDate(driver).sendKeys("1/7/2017");
		reason(driver).sendKeys("I'm too casual to come in to work");
		save(driver).click();
	}
	
	public static void returnHome(WebDriver driver){
		home(driver).click();
	}

}
