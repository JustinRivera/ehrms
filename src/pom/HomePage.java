package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	public static WebElement em(WebDriver driver){
		return driver.findElement(By.xpath("//*[@id=\"accordion\"]/div[2]/table/tbody/tr/td[2]/a"));
	}
	
	
	public static WebElement tsm(WebDriver driver){
		return driver.findElement(By.xpath("//*[@id=\"accordion\"]/div[3]/table/tbody/tr/td[2]/a"));
	}
	
	public static WebElement log(WebDriver driver){
		return driver.findElement(By.xpath("/html/body/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[3]/font/a/img"));
	}
	
	public static void openEmployeeManagement(WebDriver driver){
		em(driver).click();
	}
	
	public static void openEmployeeTimeSheet(WebDriver driver){
		tsm(driver).click();
	}
	
	public static void logout(WebDriver driver){
		log(driver).click();
	}
	
}
