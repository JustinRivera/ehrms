package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	public static WebElement user(WebDriver driver) {
		return driver.findElement(By.name("j_username"));
	}
	
	public static WebElement password(WebDriver driver) {
		return driver.findElement(By.name("j_password"));
	}
	
	public static WebElement login(WebDriver driver) {
		return driver.findElement(By.xpath("//html/body/table/tbody/tr[2]/td/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[5]/td[2]/input[1]"));
	}
	
	public static void loginEHRMS(WebDriver driver) {
		user(driver).clear();
		user(driver).sendKeys("guest");
		password(driver).sendKeys("off2hand");
		login(driver).click();
	}
	
}
