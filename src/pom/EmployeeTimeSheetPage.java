package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EmployeeTimeSheetPage {
	
	public static WebElement timeSheetAdd(WebDriver driver){
		return driver.findElement(By.xpath("//*[@id=\"dt_example\"]/table/tbody/tr[2]/td[2]/table/tbody/tr/td/table/tbody/tr[3]/td/p/a/font"));
	}
	
	public static WebElement date(WebDriver driver) {
		return driver.findElement(By.name("date"));
	}
	
	public static WebElement hours(WebDriver driver) {
		return driver.findElement(By.name("hours"));
	}
	
	public static WebElement jobDone(WebDriver driver) {
		return driver.findElement(By.id("jobDone"));
	}
	
	public static WebElement remarks(WebDriver driver){
		return driver.findElement(By.xpath("//*[@id=\"newemp\"]/div/table/tbody/tr[8]/td[2]/textarea"));
	}
	
	public static WebElement save(WebDriver driver){
		return driver.findElement(By.name("save"));
	}
	
	public static WebElement home(WebDriver driver){
		return driver.findElement(By.xpath("//*[@id=\"newemp\"]/div/table/tbody/tr[1]/td/a"));
	}
	
	public static void addTimeSheet(WebDriver driver){
		timeSheetAdd(driver).click();
		date(driver).sendKeys("1/5/2017");
		hours(driver).sendKeys("2");
		jobDone(driver).sendKeys("I've done it all");
		remarks(driver).sendKeys("Everyone can go home now");
		save(driver).click();
	}
	
	public static void returnHome(WebDriver driver){
		home(driver).click();
	}
	
}
