package pom;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class EHRMS {

	private static WebDriver driver = null;
	
	public static void main(String[] args) {
		WebDriver driver = launchApp();
		LoginPage.loginEHRMS(driver);
		HomePage.openEmployeeManagement(driver);
		EmployeeManagementPage.addNewLeave(driver);
		EmployeeManagementPage.returnHome(driver);
		HomePage.openEmployeeTimeSheet(driver);
		EmployeeTimeSheetPage.addTimeSheet(driver);
		EmployeeTimeSheetPage.returnHome(driver);
		HomePage.logout(driver);
		closeApp(driver);
	}
	
	public static WebDriver launchApp(){
		System.setProperty("webdriver.chrome.driver", "C:/selenium/chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(2,  TimeUnit.SECONDS);
		driver.get("http://192.168.60.11:7001/eHRMS/logout.do");
		
		return driver;
	}
	
	public static void closeApp(WebDriver driver){
		driver.close();
	}
}
