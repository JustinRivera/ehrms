package selenium;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MercuryTours {
	
	public static void main(String args[]){
		File file = new File("C:/selenium/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		WebDriver driver = new ChromeDriver();
		
		login(driver);
		if (driver.getTitle().equals("Find a Flight: Mercury Tours:")){ 
			flightSearch(driver);
		}
		else {
			System.out.println(driver.getTitle());
		}
		isWorking(driver);
		driver.quit();

	}
	
	public static void login(WebDriver driver){
		driver.get("http://newtours.demoaut.com/mercurywelcome.php");
		driver.findElement(By.name("userName")).sendKeys("CashMoney");
		driver.findElement(By.name("password")).sendKeys("password");
		driver.findElement(By.name("login")).sendKeys(Keys.RETURN);
	}
	
	public static void flightSearch(WebDriver driver){
		driver.findElement(By.name("findFlights")).sendKeys(Keys.RETURN);
		driver.findElement(By.name("reserveFlights")).sendKeys(Keys.RETURN);
		
		List<WebElement> flight = driver.findElements(By.className("data_left"));
		List<WebElement> cls = driver.findElements(By.className("data_center_mono"));
		List<WebElement> price = driver.findElements(By.className("data_center"));
				
		for(int i = 0; i < flight.size(); i++){
			if(i >= cls.size()){
				System.out.println(flight.get(i).getText());
			}
			else if (i >= price.size()){
				System.out.println(flight.get(i).getText() + "   " + cls.get(i).getText());
			}
			else {
				System.out.println(flight.get(i).getText() + "   " + cls.get(i).getText() + "   $" + price.get(i).getText());
			}
		}
	}

	public static void isWorking(WebDriver driver){
		for (int i = 0; i < driver.findElements(By.tagName("a")).size(); i++){
			driver.get("http://newtours.demoaut.com/mercurywelcome.php");
			driver.findElements(By.tagName("a")).get(i).click();
			System.out.print("Trying " + driver.findElements(By.tagName("a")).get(i).getText());
			
			if(driver.getTitle().equals("Under Construction: Mercury Tours")){
				System.out.println("; it is not working");
			}
			else {System.out.println();}
		}
	}
}