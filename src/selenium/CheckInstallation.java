package selenium;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckInstallation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// For Firefox
		/*File file = new File("C:/selenium/geckodriver.exe");
		System.setProperty("webdriver.gecko.driver", file.getAbsolutePath());
		
		WebDriver driver = new FirefoxDriver();
		driver.get("http://www.google.com");
		driver.close();*/
		
		// For Chrome
		File file = new File("C:/selenium/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		WebDriver driver = new ChromeDriver();
		
		driver.get("http://www.google.com");
		driver.quit();
		
		// For IE
		/*File file = new File("C:/selenium/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		WebDriver driver = new InternetExplorerDriver();
		
		driver.get("http://www.google.com");
		driver.quit();*/
		
	}

}
